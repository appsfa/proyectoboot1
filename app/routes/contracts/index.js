import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

  model(){
    return RSVP.hash({
      contracts: this.store.findAll('contract'),
      subscribers: this.store.findAll('subscriber')
    })
  }

});
