import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

  model(){
    return RSVP.hash({
      lecture: this.store.findAll('lecture'),
      teachers: this.store.findAll('teacher'),
      students: this.store.findAll('student')
    })
  }
});
