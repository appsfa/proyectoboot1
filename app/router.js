import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login', {path: '/'});
  this.route('dashboard');
  this.route('students', function() {
    this.route('perfil',{path: '/perfil/:student_id'});
  });
  this.route('teachers', function() {
    this.route('profile',{path: '/profile/:teacher_id'});
  });
  this.route('lessons');
  this.route('levels');
  this.route('courses');
  this.route('subscribers', function() {
    this.route('profile',{path: '/profile/:subscriber_id'});
  });
  this.route('contracts', function() {
    this.route('profile',{path: '/profile/:contract_id'});
  });
  this.route('activity');
  this.route('lecture', function() {
    this.route('profile',{path: '/profile/:lecture_id'});
  });
});

export default Router;
