import Controller from '@ember/controller';

export default Controller.extend({

  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLectures(){
      this.transitionToRoute('lecture');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },
    goStudentDetails(student){
      this.transitionToRoute('students.perfil', student.id);
    },

    addStudent(){
    this.store.createRecord('student',{
      name: this.get('name'),
      lastName: this.get('lastName'),
      email: this.get('email'),
      phone: this.get('phone'),
      test_grade: this.get('test_grade')
    }).save().then(()=>{
      this.set('name', ''),
      this.set('lastName',''),
      this.set('email',''),
      this.set('phone',''),
      this.set('test_grade',''),
      alert("La información del alumno ha sido actualizada.")
    })

    },
    Cancel(){
      if(confirm("¿Seguro de que quieres salir sin guardar?")){
        this.set('name', ''),
        this.set('lastName',''),
        this.set('email',''),
        this.set('phone',''),
        this.set('test_grade','')
      }
    },

    deleteStudent(student){
      if(confirm("¿Estas seguro que lo deseas eliminar?"))
    {
      student.destroyRecord().then(() => {
        this.transitionToRoute('students')
      }).catch((e) => {
      })
    }
    },

    updateStudent(student){
      if(confirm("Estás seguro que deseas actualizar a este estudiante?")){
        student.save();
      }
    }
  }

});
