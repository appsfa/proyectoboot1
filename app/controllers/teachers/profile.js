import Controller from '@ember/controller';

export default Controller.extend({

    actions:{
      goStudents(){
        this.transitionToRoute('students');
      },
      goTeachers(){
        this.transitionToRoute('teachers');
      },
      goLessons(){
        this.transitionToRoute('lessons');
      },
      goLectures(){
        this.transitionToRoute('lecture');
      },

      goActivity(){
        this.transitionToRoute('activity');
      },
      goHome(){
        this.transitionToRoute('dashboard');
      },
      goSubscribers(){
        this.transitionToRoute('subscribers');
      },
      goContracts(){
        this.transitionToRoute('contracts');
      },
      goLogin(){
        this.transitionToRoute('login');
      },
      goProfileTeacher(teacher){
        this.transitionToRoute('teachers.profile', teacher.id);
      },

      addTeacher(){
      this.store.createRecord('teacher',{
        name: this.get('name'),
        lastName: this.get('lastName'),
        mail: this.get('mail'),
        phone: this.get('phone')
      }).save().then(()=>{
        this.set('name', ''),
        this.set('lastName',''),
        this.set('email',''),
        this.set('phone',''),
        alert("La información del profesor ha sido actualizada.")
      })

      },
      Cancel(){
        if(confirm("¿Seguro de que quieres salir sin guardar?")){
          this.set('name', ''),
          this.set('lastName',''),
          this.set('mail',''),
          this.set('phone',''),
          this.get('model').rollbackAttributes();

        }
      },

      deleteTeacher(teacher){
        if(confirm("¿Estas seguro que lo deseas eliminar?"))
      {
        teacher.destroyRecord().then(() => {
          this.transitionToRoute('teachers')
        }).catch((e) => {

        })
      }
      },

      updateTeacher(teacher){
        if(confirm("Estás seguro que deseas actualizar a este profesor?")){
          teacher.save().then(()=>{
          });
        }
      }
    }
});
