import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLevels(){
      this.transitionToRoute('levels');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },

  addTeacher(){
  this.store.createRecord('teacher',{
    name: this.get('name'),
    lastName: this.get('lastName'),
    email: this.get('email'),
    phoneNumber: this.get('phoneNumber')
  }).save().then(()=>{
    alert("La información del profesor ha sido creada.")
  })

  },
  CancelButton(){
    if(confirm("¿Seguro de que quieres salir sin guardar?")){
      this.get('model').rollbackAttributes();

    }
  },

  deleteTeacher(teacher){
    if(confirm("¿Estas seguro que lo deseas eliminar?"))
  {
    teacher.destroyRecord().then(() => {
      this.transitionToRoute('teachers')
    }).catch(() => {

    })
  }
  },

  updateTeacher(teacher){
    if(confirm("Estás seguro que deseas actualizar a este profesor?")){
      teacher.save();
    }
  }
}
});
