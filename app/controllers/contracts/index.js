import Controller from '@ember/controller';

export default Controller.extend({
  subscriberSelected: null,
  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLectures(){
      this.transitionToRoute('lecture');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },
    goContractProfile(contract){
      this.transitionToRoute('contracts.profile', contract.id);
    },

    addContract(){
    let contract = this.store.createRecord('contract',{
      contractSignatureDate: new Date(this.get('contractSignatureDate')),
      contractDueDate: new Date (this.get('contractDueDate')),
    })
    contract.set('subscriber', this.get('subscriberSelected'))
    contract.save().then(()=>{
      this.set('contractSignatureDate', ''),
      this.set('contractDueDate', ''),
      alert("La información del contrato ha sido actualizada.")
    })

    },
    Cancel(){
      if(confirm("¿Seguro de que quieres salir sin guardar?")){
        this.set('contractSignatureDate', ''),
        this.set('contractDueDate', '')
      }
    },

    deleteContract(contract){
      if(confirm("¿Estas seguro que lo deseas eliminar?"))
    {
      contract.destroyRecord().then(() => {
        this.transitionToRoute('contracts')
      }).catch((e) => {

      })
    }
    },

    updateContract(contract){
      if(confirm("Estás seguro que deseas actualizar a este contrato?")){
        contract.save();
      }
    },

    onSubscriberSelected(subscriber){

      this.set('subscriberSelected',subscriber)

    }
  }
});
