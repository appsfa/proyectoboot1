import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLevels(){
      this.transitionToRoute('levels');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },

    addSubscriber(){
    this.store.createRecord('subscriber',{
      name: this.get('name'),
      last_Name: this.get('last_Name'),
      email: this.get('email'),
      phoneNumber: this.get('phoneNumber')
    }).save().then(()=>{
      alert("La información del suscriptor ha sido actualizada.")
    })

    },
    CancelButton(){
      if(confirm("¿Seguro de que quieres salir sin guardar?")){
        this.get('model').rollbackAttributes();

      }
    },

    deleteSubscriber(subscriber){
      if(confirm("¿Estas seguro que lo deseas eliminar?"))
    {
      subscriber.destroyRecord().then(() => {
        this.transitionToRoute('subscribers')
      }).catch((e) => {

      })
    }
    },

    updateSubscriber(subscriber){
      if(confirm("Estás seguro que deseas actualizar a este suscriptor?")){
        subscriber.save();
      }
    }
  }
});
