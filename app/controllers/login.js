import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    verifyUser(){
      if (this.get('user') === 'A01271806' && this.get('pass') === 'A01271806')
      {
        this.transitionToRoute('dashboard');
      } else{
        $("#infoLogin").html('<i class="material-icons red-text">close</i> Usuario o contraseña incorrecta<br><br>');
        $("#txtUser").addClass('invalid');
        $("#txtPass").addClass('invalid');
      }
      this.set('user', '');
      this.set('pass', '');

    }
  }
});
