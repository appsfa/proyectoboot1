import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLevels(){
      this.transitionToRoute('levels');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },

    addLesson(){
    this.store.createRecord('lesson',{
      name: this.get('name'),
    }).save().then(()=>{
      alert("La información de la lección ha sido actualizada.")
    })

    },
    CancelButton(){
      if(confirm("¿Seguro de que quieres salir sin guardar?")){
        this.get('model').rollbackAttributes();

      }
    },

    deleteLesson(lesson){
      if(confirm("¿Estas seguro que deseas eliminar esta lección?"))
    {
      lesson.destroyRecord().then(() => {
        this.transitionToRoute('lessons')
      }).catch((e) => {

      })
    }
    },

    updateLesson(lesson){
      if(confirm("Estás seguro que deseas actualizar a esta lección?")){
        lesson.save();
      }
    }

  }
});
