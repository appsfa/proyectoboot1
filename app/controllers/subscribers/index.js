import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLectures(){
      this.transitionToRoute('lecture');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },
    goSubscriberProfile(subscriber){
      this.transitionToRoute('subscribers.profile', subscriber.id);
    },


    addSubscriber(){
    this.store.createRecord('subscriber',{
      name: this.get('name'),
      last_name: this.get('last_name'),
      mail: this.get('mail'),
      phone: this.get('phone')
    }).save().then(()=>{
      this.set('name', ''),
      this.set('last_Name',''),
      this.set('mail',''),
      this.set('phone',''),
      alert("La información del suscriptor ha sido actualizada.")
    })

    },
    CancelButton(){
      if(confirm("¿Seguro de que quieres salir sin guardar?")){
        this.set('name', ''),
        this.set('last_Name',''),
        this.set('mail',''),
        this.set('phone',''),
        this.get('model').rollbackAttributes();
      }
    },

    deleteSubscriber(subscriber){
      if(confirm("¿Estas seguro que lo deseas eliminar?"))
    {
      subscriber.destroyRecord().then(() => {
        this.transitionToRoute('subscribers')
      }).catch((e) => {

      })
    }
    },

    updateSubscriber(subscriber){
      if(confirm("Estás seguro que deseas actualizar a este suscriptor?")){
        subscriber.save();
      }
    },
    onSubscriberSelected(){

    }
  }
});
