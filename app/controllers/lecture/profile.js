import Controller from '@ember/controller';

export default Controller.extend({

  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLectures(){
      this.transitionToRoute('lecture');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },
    goLectureProfile(lecture){
      this.transitionToRoute('lecture.profile', lecture.id);
    },

    addLecture(){
    this.store.createRecord('lecture',{
      startDate: this.get('startDate'),
      classroom: this.get('classroom'),
    }).save().then(()=>{
      this.set('startDate',''),
      this.set('classroom',''),
      alert("La información del alumno ha sido actualizada.")
    })

    },
    CancelButton(){
      if(confirm("¿Seguro de que quieres salir sin guardar?")){

      }
    },

    deleteLecture(lecture){
      if(confirm("¿Estas seguro que lo deseas eliminar?"))
    {
      lecture.destroyRecord().then(() => {
        this.transitionToRoute('lectures')
      }).catch((e) => {
      })
    }
    },

    updateLecture(lecture){
      if(confirm("Estás seguro que deseas actualizar a este estudiante?")){
        lecture.save();
      }
    }
  }

});
