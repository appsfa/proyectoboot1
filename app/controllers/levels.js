import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    goStudents(){
      this.transitionToRoute('students');
    },
    goTeachers(){
      this.transitionToRoute('teachers');
    },
    goLessons(){
      this.transitionToRoute('lessons');
    },
    goLevels(){
      this.transitionToRoute('levels');
    },

    goActivity(){
      this.transitionToRoute('activity');
    },
    goHome(){
      this.transitionToRoute('dashboard');
    },
    goSubscribers(){
      this.transitionToRoute('subscribers');
    },
    goContracts(){
      this.transitionToRoute('contracts');
    },
    goLogin(){
      this.transitionToRoute('login');
    },

    addLevel(){
    this.store.createRecord('level',{
      name: this.get('name'),
    }).save().then(()=>{
      alert("La información de la lección ha sido actualizada.")
    })

    },
    CancelButton(){
      if(confirm("¿Seguro de que quieres salir sin guardar?")){
        this.get('model').rollbackAttributes();

      }
    },

    deleteLevel(level){
      if(confirm("¿Estas seguro que deseas eliminar este nivel?"))
    {
      level.destroyRecord().then(() => {
        this.transitionToRoute('levels')
      }).catch((e) => {

      })
    }
    },

    updateLevel(level){
      if(confirm("Estás seguro que deseas actualizar a este nivel?")){
        level.save();
      }
    }

  }
});
