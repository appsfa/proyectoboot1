import DS from 'ember-data';

export default DS.Model.extend({


  lecture: DS.belongsTo('lecture'),
  activity: DS.belongsTo('activity'),
  student: DS.belongsTo('student'),
  grade: DS.attr('number', {defaultValue: 0}),
  status: DS.attr('boolean')


});
