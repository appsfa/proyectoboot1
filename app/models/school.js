import DS from 'ember-data';

export default DS.Model.extend({

  name: DS.attr('string'),
  scheduleStart: DS.attr('string'),
  scheduleEnd: DS.attr('string'),
  rules: DS.hasMany('rule'),
  lectures: DS.hasMany('lecture'),
  levels: DS.hasMany('level')

});
