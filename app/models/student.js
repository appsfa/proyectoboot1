import DS from 'ember-data';

export default DS.Model.extend({

  activity: DS.belongsTo('activity'),
  name: DS.attr('string'),
  lastName: DS.attr('string'),
  phone: DS.attr('string'),
  email: DS.attr('string'),
  test_grade: DS.attr('number'),
  reservations: DS.hasMany('reservation'),
  contracts: DS.hasMany('student-contract')

});
