import DS from 'ember-data';

export default DS.Model.extend({

  lesson: DS.belongsTo('lesson'),
  activityType: DS.belongsTo('activity-type'),
  students: DS.hasMany('student'),
  reservations: DS.hasMany('reservation')

});
