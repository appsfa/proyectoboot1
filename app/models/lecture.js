import DS from 'ember-data';

export default DS.Model.extend({

  startDate: DS.attr('string'),
  classroom: DS.attr('string'),
  school: DS.belongsTo('school'),
  teacher: DS.belongsTo('teacher'),
  student: DS.belongsTo('student'),
  reservations: DS.hasMany('reservation')

});
