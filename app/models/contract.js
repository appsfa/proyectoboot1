import DS from 'ember-data';

export default DS.Model.extend({

    subscriber: DS.belongsTo('subscriber'),
    contractSignatureDate: DS.attr('date'),
    studentContracts: DS.hasMany('student-contract'),
    contractDueDate: DS.attr('date')

});
