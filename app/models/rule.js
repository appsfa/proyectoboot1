import DS from 'ember-data';

export default DS.Model.extend({

  maxStudents: DS.attr('string'),
  school: DS.belongsTo('school'),
  activitiesRules: DS.hasMany('activity-rule')

});
