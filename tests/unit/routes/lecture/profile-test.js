import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | lecture/profile', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:lecture/profile');
    assert.ok(route);
  });
});
