import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | contracts/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:contracts/index');
    assert.ok(route);
  });
});
